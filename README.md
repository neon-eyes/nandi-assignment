
# Nandi Assignment

### API Used: [ExchangeRate-API](https://www.exchangerate-api.com/)

Default base currency is set to ILS (inside the Dockerfile).\
To override the value, use **--build-arg baseCurrency=`<any currency>`** during the build stage.

## Access Key is sent privately. 
Use **--build-arg accessKey=`<access key>`** during the build stage 

#### Commands:
Build image: \
`docker build <--build-arg baseCurrency=GBP> --build-arg accessKey=<access key> -t <name of image> .`

Run container: \
`docker run -t --name <container name> <image>`
