import requests
import os

ACCESS_KEY = os.environ.get('ACCESS_KEY')
BASE_CUR = os.environ.get('BASE_CUR')

url = f"https://v6.exchangerate-api.com/v6/{ACCESS_KEY}/latest/{BASE_CUR}"
response = requests.get(url)
data = response.json()

print(f"Base Currency: {data['base_code']}")
print(f"Conversion Rates:\n  USD: {data['conversion_rates']['USD']}\n  EUR: {data['conversion_rates']['EUR']}")
