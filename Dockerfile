FROM python:latest

# args that are getting overwritten when building the image
# to overwrite/assign add: --build-arg var_name=VARIABLE_NAME for each arg
ARG baseCurrency=ILS
ARG accessKey

ENV ACCESS_KEY=${accessKey} 
ENV BASE_CUR=${baseCurrency}

WORKDIR /home/py-app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD [ "python", "main.py" ]